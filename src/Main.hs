module Main where

import Parser

main :: IO ()
main = do
  print $ parseExpr "foo > 42"
  print $ parseExpr "foo > 42 or bar <> false"
  print $ parseExpr "(foo > 42)"
  print $ parseExpr "(foo > 42) or (bar <> false)"
  print $ parseExpr "(foo > 42) and (true <> false or baz = qux)"
  print $ parseExpr "(true <> false or baz = qux) or (foo > 42)"
  print $ parseExpr "((true <> false or baz = qux) or (foo > 42))"
