module Parser where

import Text.Parsec
import Text.Parsec.String (Parser)

import qualified Text.Parsec.Expr as Ex

import Lexer
import Syntax

term :: Parser Term
term = number
    <|> quoted
    <|> selector

condition :: Parser Condition
condition = complex <|> parens condition
  where
    simple = do
      left <- term
      op <- binop
      right <- term
      return (Simple left op right)
    complex = Ex.buildExpressionParser operators operand
    operand = simple <|> parens complex
    operators = [ [binary "and" And]
                , [binary "or" Or]
                ]
    binary s f = Ex.Infix (reserved s >> return (\l r -> Complex l f r)) Ex.AssocLeft

binop :: Parser Op
binop = (reservedOp "=" >> return Eq)
     <|> (reservedOp "!=" >> return Neq)
     <|> try (reservedOp "<>" >> return Neq)
     <|> try (reservedOp "<=" >> return Lte)
     <|> (reservedOp "<" >> return Lt)
     <|> try (reservedOp ">=" >> return Gte)
     <|> (reservedOp ">" >> return Gt)
     <|> (reservedOp "in" >> return In)
     <|> (reservedOp "is" >> return Is)

number :: Parser Term
number = do
  n <- integer
  return $ Number (fromInteger n)

quoted :: Parser Term
quoted = do
  s <- stringLiteral
  return $ Quoted s

selector :: Parser Term
selector = do
  parts <- identifier `sepBy1` dot
  return $ case parts of
    [const] -> Const const
    _       -> Selector parts

contents :: Parser a -> Parser a
contents p = do
  whiteSpace
  r <- p
  eof
  return r

parseExpr :: String -> Either ParseError Condition
parseExpr s = parse (contents condition) "<expr>" s

-- vim: ts=2 sw=2
