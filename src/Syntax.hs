module Syntax where

type Identifier = String

data Term
  = Number Integer
  | Quoted String
  | Selector [Identifier]
  | Const Identifier
  deriving (Eq, Ord, Show)

data Condition
  = Simple Term Op Term
  | Complex Condition LogicalOp Condition
  deriving (Eq, Ord, Show)

data LogicalOp
  = And
  | Or
  deriving (Eq, Ord, Show)

data Op
  = Eq
  | Neq
  | Gt | Gte
  | Lt | Lte
  | In
  | Is
  deriving (Eq, Ord, Show)

-- vim: sw=2 ts=2
