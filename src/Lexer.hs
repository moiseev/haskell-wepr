module Lexer where

import Text.Parsec.String (Parser)
import Text.Parsec.Language (emptyDef)

import qualified Text.Parsec.Token as Tok

lexer :: Tok.TokenParser ()
lexer = Tok.makeTokenParser style
  where
    ops = [ "=", "<>", "in", "is"
          , "<", "<=", ">", ">="
          ]
    keywords = ["and", "or"]
    style = emptyDef
      { Tok.caseSensitive = False
      , Tok.reservedNames = keywords
      , Tok.reservedOpNames = ops
      }

integer :: Parser Integer
integer = Tok.integer lexer

stringLiteral :: Parser String
stringLiteral = Tok.stringLiteral lexer

parens :: Parser a -> Parser a
parens = Tok.parens lexer

reserved :: String -> Parser ()
reserved = Tok.reserved lexer

reservedOp :: String -> Parser ()
reservedOp = Tok.reservedOp lexer

identifier :: Parser String
identifier = Tok.identifier lexer

dot :: Parser String
dot = Tok.dot lexer

whiteSpace :: Parser ()
whiteSpace = Tok.whiteSpace lexer

-- vim: ts=2 sw=2
