# WEPR

Simple parser for SQL-like `where` expressions using `Parsec`

## Valid expressions

Valid expression can contain:

- Identifiers

- Selectors (a dot-separated list of identifiers)

- String literals (using double quotes)

- Integers

- Equality operators (usual suspects: `=`, `<>`, `<=`, etc.)

Sub-expressions can be combined using logical operators `or` and `and`. (`and`
has higher precendence). Parentheses are allowed around sub-expressions to
change priority.

## How to build

First of all have `stack` installed (find out about `stack`
[here](http://docs.haskellstack.org/)).

After that simply:

````sh
stack build && stack exec wepr
````

No tests, I know...
